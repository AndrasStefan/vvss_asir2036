package asir2036MV.main;

import asir2036MV.controller.AppController;
import asir2036MV.repository.IntrebariRepository;
import asir2036MV.util.InputValidation;
import asir2036MV.view.UserInterface;

//functionalitati
//F01.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//F02.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//F03.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {

    private static final String file = "intrebari.txt";

    public static void main(String[] args) {

        try {
            IntrebariRepository intrebariRepository = new IntrebariRepository(file);
            InputValidation inputValidation = new InputValidation();
            AppController appController = new AppController(intrebariRepository, inputValidation);
            UserInterface userInterface = new UserInterface(appController);
            userInterface.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
