package asir2036MV.util;


import asir2036MV.exception.InputValidationFailedException;
import asir2036MV.model.Intrebare;

public class InputValidation {

    public static boolean validateIntrebare(Intrebare intrebare) throws InputValidationFailedException {

        String message = "";
        if (intrebare.getDomeniu().equals("") || intrebare.getEnunt().equals("") || intrebare.getVarianta1().equals("") || intrebare.getVarianta2().equals("") || intrebare.getVarianta3().equals("") || intrebare.getVariantaCorecta().equals(""))
            message = message + " Campurile nu pot fi vide.";

        if (!Character.isUpperCase(intrebare.getDomeniu().charAt(0)) || !Character.isUpperCase(intrebare.getEnunt().charAt(0)) || !Character.isUpperCase(intrebare.getVarianta1().charAt(0)) || !Character.isUpperCase(intrebare.getVarianta2().charAt(0)) || !Character.isUpperCase(intrebare.getVarianta3().charAt(0)))
            message = message + " Prima litera trebuie sa fie majuscula.";

        if (!String.valueOf(intrebare.getEnunt().charAt(intrebare.getEnunt().length() - 1)).equals("?"))
            message = message + " Ultimul caracter din intrebare nu e '?'.";

        if (intrebare.getVarianta1().length() > 100 || intrebare.getVarianta2().length() > 100 || intrebare.getVarianta3().length() > 100 || intrebare.getEnunt().length() > 100)
            message = message + "Lungimea depaseste 100 de caractere!";

        if (!intrebare.getDomeniu().equals("Organe interne") && !intrebare.getDomeniu().equals("Celule") && !intrebare.getDomeniu().equals("Alimentatie sanatoasa") && !intrebare.getDomeniu().equals("Organe externe") && !intrebare.getDomeniu().equals("Boli")) {
            message = message + " Domeniul nu exista.";
        }
        if (!intrebare.getVariantaCorecta().equals("1") && !intrebare.getVariantaCorecta().equals("2") && !intrebare.getVariantaCorecta().equals("3")) {
            message = message + " Varianta corecta poate fi 1,2 sau 3.";
        }
        if (message != "") {
            throw new InputValidationFailedException(message);
        }
        return true;

    }
}
