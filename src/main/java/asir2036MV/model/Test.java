package asir2036MV.model;

import java.util.LinkedList;
import java.util.List;

public class Test {

    private List<Intrebare> intrebari;

    public Test() {
        intrebari = new LinkedList<Intrebare>();
    }

    public List<Intrebare> getIntrebari() {
        return intrebari;
    }

    public void setIntrebari(List<Intrebare> intrebari) {
        this.intrebari = intrebari;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Intrebare i : intrebari) {
            sb.append(i.getDomeniu()).append("\n");
            sb.append(i.getEnunt()).append("\n");
            sb.append("1. ");
            sb.append(i.getVarianta1()).append("\n");
            sb.append("2. ");
            sb.append(i.getVarianta2()).append("\n");
            sb.append("3. ");
            sb.append(i.getVarianta3()).append("\n");
        }
        return String.valueOf(sb);
    }
}
