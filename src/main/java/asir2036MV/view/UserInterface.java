package asir2036MV.view;

import asir2036MV.controller.AppController;
import asir2036MV.exception.DuplicateIntrebareException;
import asir2036MV.exception.InputValidationFailedException;
import asir2036MV.exception.NotAbleToCreateStatisticsException;
import asir2036MV.exception.NotAbleToCreateTestException;
import asir2036MV.model.Intrebare;
import asir2036MV.model.Statistica;
import asir2036MV.model.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class UserInterface {
    private AppController appController;
    public BufferedReader bufferedReader;

    public UserInterface(AppController appController) {
        this.appController = appController;
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    }

    private void menu() {
        System.out.println("");
        System.out.println("1.Adauga intrebare");
        System.out.println("2.Creeaza test");
        System.out.println("3.Statistica");
        System.out.println("4.Exit");
        System.out.println("");
    }

    public void show() {
        String cmd;

        menu();


        while (true) {
            try {
                cmd = bufferedReader.readLine();
                if (cmd.equals("4"))
                    break;
                else if (cmd.equals("1")) {
                    try {
                        System.out.println("Introduceti intrebarea :  ");
                        String intrebare = bufferedReader.readLine();
                        System.out.println("Introduceti textul raspunsului pentru varianata 1 : ");
                        String varianta1 = bufferedReader.readLine();
                        System.out.println("Introduceti textul raspunsului pentru varianata 2 : ");
                        String varianta2 = bufferedReader.readLine();
                        System.out.println("Introduceti textul raspunsului pentru varianata 3 : ");
                        String varianta3 = bufferedReader.readLine();
                        System.out.println("Introduceti numarul raspunsului corect ( 1, 2 sau 3 ) : ");
                        String rspunsCorect = bufferedReader.readLine();
                        System.out.println("Domeniile sunt Organe interne, Celule, Alimentatie sanatoasa, Organe externe, Boli. Introdu numele domeniului dorit : ");
                        String domeniu = bufferedReader.readLine();


                        Intrebare intrebareIntreaga = new Intrebare(intrebare, varianta1, varianta2, varianta3, rspunsCorect, domeniu);
                        try {
                            if(appController.addNewIntrebare(intrebareIntreaga)!= null){
                                System.out.println("Added");
                            }
                            else
                                System.out.println("Not added.");
                        } catch (DuplicateIntrebareException e) {
                            System.out.println(e.getMessage());
                        } catch (InputValidationFailedException e) {
                            System.out.println(e.getMessage());
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (cmd.equals("2")) {
                    try {
                        Test test = appController.createNewTest();
                        System.out.println("Testul generat este:" + test.toString());
                    } catch (NotAbleToCreateTestException e) {
                        System.out.println(e.getMessage());
                    } catch (NumberFormatException e) {
                        System.out.println("id-ul si salariul trebuie sa fie numere");
                    } catch (IllegalArgumentException e) {
                        System.out.println("Nu exista functia");
                    } catch (RuntimeException e) {
                        System.out.println("Nu exista acest angajat cu acest id");
                    }

                } else if (cmd.equals("3")) {
                    Statistica statistica;
                    try {
                        statistica = appController.getStatistica();
                        System.out.println("Statistica generata este:"+ statistica);
                    } catch (NotAbleToCreateStatisticsException e) {
                        System.out.println(e.getMessage());
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

