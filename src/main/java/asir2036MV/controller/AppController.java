package asir2036MV.controller;

import asir2036MV.exception.DuplicateIntrebareException;
import asir2036MV.exception.InputValidationFailedException;
import asir2036MV.exception.NotAbleToCreateStatisticsException;
import asir2036MV.exception.NotAbleToCreateTestException;
import asir2036MV.model.Intrebare;
import asir2036MV.model.Statistica;
import asir2036MV.model.Test;
import asir2036MV.repository.IntrebariRepository;
import asir2036MV.util.InputValidation;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class AppController {

    private IntrebariRepository intrebariRepository;
    private InputValidation inputValidation;

    public AppController(IntrebariRepository intrebariRepository, InputValidation inputValidation) {
        this.intrebariRepository = intrebariRepository;
        this.inputValidation = inputValidation;
    }

    public Intrebare addNewIntrebare(Intrebare intrebare) throws DuplicateIntrebareException, InputValidationFailedException {

        try {
            intrebariRepository.exists(intrebare);
            inputValidation.validateIntrebare(intrebare);
            intrebariRepository.addIntrebare(intrebare);
        } catch (InputValidationFailedException ex) {
            throw new InputValidationFailedException(ex.getMessage());
        } catch (DuplicateIntrebareException ex) {
            throw new DuplicateIntrebareException(ex.getMessage());
        } catch (IOException e) {
            e.getMessage();
        }


        return intrebare;
    }

    public Test createNewTest() throws NotAbleToCreateTestException {
        List<Intrebare> testIntrebari = new LinkedList<Intrebare>();
        List<String> domenii = new LinkedList<String>();
        Intrebare intrebare;
        Test test = new Test();

        if (intrebariRepository.getIntrebari().size() < 5) {
            throw new NotAbleToCreateTestException("Nu exista suficiente intrebari pentru crearea unui test!(5)");
        }
        
        if (intrebariRepository.getNumberOfDistinctDomains() < 5)
            throw new NotAbleToCreateTestException("Nu exista suficiente domenii pentru crearea unui test!(5)");

        while (testIntrebari.size() != 5) {
            intrebare = intrebariRepository.pickRandomIntrebare();

            if (!testIntrebari.contains(intrebare) && !domenii.contains(intrebare.getDomeniu())) {
                testIntrebari.add(intrebare);
                domenii.add(intrebare.getDomeniu());
            }

        }

        test.setIntrebari(testIntrebari);
        return test;
    }

    public Statistica getStatistica() throws NotAbleToCreateStatisticsException {

        if (intrebariRepository.getIntrebari().isEmpty())
            throw new NotAbleToCreateStatisticsException("Repository-ul nu contine nicio intrebare!");

        Statistica statistica = new Statistica();
        Set<String> domains= intrebariRepository.getDistinctDomains();
        for (String domain: domains) {
            Integer number= intrebariRepository.getNumberOfIntrebariByDomain(domain);
            statistica.add(domain,number);
        }

        return statistica;
    }

}
