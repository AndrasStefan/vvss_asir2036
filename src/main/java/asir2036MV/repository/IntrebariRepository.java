package asir2036MV.repository;

import asir2036MV.exception.DuplicateIntrebareException;
import asir2036MV.model.Intrebare;

import java.io.*;
import java.util.*;

public class IntrebariRepository {

    private List<Intrebare> intrebari;
    private String file;

    public IntrebariRepository(String file) throws Exception {
        setIntrebari(new LinkedList<Intrebare>());
        this.file = file;
        this.loadIntrebariFromFile(file);
    }

    public void addIntrebare(Intrebare i) throws IOException {
        this.writeIntrebareToFile(file, i);
        intrebari.add(i);
    }

    private void writeIntrebareToFile(String f, Intrebare intrebare) throws IOException {
        File file = new File(f);

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        FileWriter fw = new FileWriter(file,true);

        PrintWriter out = new PrintWriter(fw);
        out.append(intrebare.getEnunt());
        out.append(",");
        out.append(intrebare.getVarianta1());
        out.append(",");
        out.append(intrebare.getVarianta2());
        out.append(",");
        out.append(intrebare.getVarianta3());
        out.append(",");
        out.append(intrebare.getVariantaCorecta());
        out.append(",");
        out.append(intrebare.getDomeniu());
        out.append("\n");

        out.close();
    }

    public boolean exists(Intrebare i) throws DuplicateIntrebareException {
        for (Intrebare intrebare : intrebari)
            if (intrebare.equals(i))
                throw new DuplicateIntrebareException("Intrebarea exista deja");
        return false;
    }

    public Intrebare pickRandomIntrebare() {
        Random random = new Random();
        return intrebari.get(random.nextInt(intrebari.size()));
    }

    public int getNumberOfDistinctDomains() {
        return getDistinctDomains().size();

    }

    public Set<String> getDistinctDomains() {
        Set<String> domains = new TreeSet<String>();
        for (Intrebare intrebre : intrebari)
            domains.add(intrebre.getDomeniu());
        return domains;
    }

    public List<Intrebare> getIntrebariByDomain(String domain) {
        List<Intrebare> intrebariByDomain = new LinkedList<Intrebare>();
        for (Intrebare intrebare : intrebari) {
            if (intrebare.getDomeniu().equals(domain)) {
                intrebariByDomain.add(intrebare);
            }
        }

        return intrebariByDomain;
    }

    public int getNumberOfIntrebariByDomain(String domain) {
        return getIntrebariByDomain(domain).size();
    }

    public void loadIntrebariFromFile(String f) throws Exception {

        BufferedReader br = null;
        String line = null;
        Intrebare intrebare;

        try {
            File yourFile = new File(f);
            yourFile.createNewFile();
            br = new BufferedReader(new FileReader(yourFile));
            line = br.readLine();

            while (line != null) {
                String[] linie= line.split(",");
                intrebare = new Intrebare();
                intrebare.setEnunt(linie[0]);
                intrebare.setVarianta1(linie[1]);
                intrebare.setVarianta2(linie[2]);
                intrebare.setVarianta3(linie[3]);
                intrebare.setVariantaCorecta(linie[4]);
                intrebare.setDomeniu(linie[5]);
                intrebari.add(intrebare);
                line = br.readLine();
            }
            try {
                br.close();
            } catch (IOException e) {
                throw new Exception("Fisierul nu se poate inchide.");
            }
        } catch (IOException e) {
            throw new Exception("Fisierul nu poate fi deschis.");
        }
    }

    public List<Intrebare> getIntrebari() {
        return intrebari;
    }

    public void setIntrebari(List<Intrebare> intrebari) {
        this.intrebari = intrebari;
    }

}
