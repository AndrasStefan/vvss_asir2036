package asir2036MV.controller;

import asir2036MV.exception.DuplicateIntrebareException;
import asir2036MV.exception.InputValidationFailedException;
import asir2036MV.exception.NotAbleToCreateTestException;
import asir2036MV.model.Intrebare;
import asir2036MV.repository.IntrebariRepository;
import asir2036MV.util.InputValidation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

public class AppControllerTest {

    private String path;
    private File file;
    private IntrebariRepository intrebariRepository;
    private AppController appController;
    private InputValidation inputValidation;


    @Before
    public void setUp() throws Exception {
        path = "test.txt";
        this.intrebariRepository = new IntrebariRepository(path);
        this.inputValidation = new InputValidation();
        this.appController = new AppController(intrebariRepository, inputValidation);
    }

    @After
    public void tearDown() throws Exception {
        file = new File(path);
        file.delete();
    }

    @Test
    public void tc1_ecp() throws InputValidationFailedException, DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Celule");
        Intrebare added = appController.addNewIntrebare(intrebare);
        assertEquals(added.getEnunt(), intrebare.getEnunt());
        assertEquals(added.getVarianta1(), intrebare.getVarianta1());
        assertEquals(added.getVarianta2(), intrebare.getVarianta2());
        assertEquals(added.getVarianta3(), intrebare.getVarianta3());
        assertEquals(added.getVariantaCorecta(), intrebare.getVariantaCorecta());
        assertEquals(added.getDomeniu(), intrebare.getDomeniu());
    }

    @Test(expected = asir2036MV.exception.InputValidationFailedException.class)
    public void tc3_ecp() throws InputValidationFailedException, DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "7", "Celule");
        Intrebare added = appController.addNewIntrebare(intrebare);
    }

    @Test(expected = asir2036MV.exception.InputValidationFailedException.class)
    public void tc5_ecp() throws DuplicateIntrebareException, InputValidationFailedException {
        Intrebare intrebare = new Intrebare("care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Celule");
        Intrebare added = appController.addNewIntrebare(intrebare);
    }

    @Test
    public void tc8_bva() throws DuplicateIntrebareException, InputValidationFailedException {
        Intrebare intrebare = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "3", "Celule");
        Intrebare added = appController.addNewIntrebare(intrebare);
        assertEquals(added.getEnunt(), intrebare.getEnunt());
        assertEquals(added.getVarianta1(), intrebare.getVarianta1());
        assertEquals(added.getVarianta2(), intrebare.getVarianta2());
        assertEquals(added.getVarianta3(), intrebare.getVarianta3());
        assertEquals(added.getVariantaCorecta(), intrebare.getVariantaCorecta());
        assertEquals(added.getDomeniu(), intrebare.getDomeniu());
    }

    @Test
    public void tc7_bva() throws DuplicateIntrebareException, InputValidationFailedException {
        Intrebare intrebare = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "2", "Celule");
        Intrebare added = appController.addNewIntrebare(intrebare);
        assertEquals(added.getEnunt(), intrebare.getEnunt());
        assertEquals(added.getVarianta1(), intrebare.getVarianta1());
        assertEquals(added.getVarianta2(), intrebare.getVarianta2());
        assertEquals(added.getVarianta3(), intrebare.getVarianta3());
        assertEquals(added.getVariantaCorecta(), intrebare.getVariantaCorecta());
        assertEquals(added.getDomeniu(), intrebare.getDomeniu());
    }

    @Test(expected = asir2036MV.exception.InputValidationFailedException.class)
    public void tc9_bva() throws DuplicateIntrebareException, InputValidationFailedException {
        Intrebare intrebare = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "4", "Celule");
        Intrebare added = appController.addNewIntrebare(intrebare);
    }

    @Test(expected = asir2036MV.exception.InputValidationFailedException.class)
    public void tc4_bva() throws DuplicateIntrebareException, InputValidationFailedException {
        Intrebare intrebare = new Intrebare("Care e celula cea mai mult raspandita", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "3", "Celule");
        Intrebare added = appController.addNewIntrebare(intrebare);
    }

    @Test(expected = asir2036MV.exception.NotAbleToCreateTestException.class)
    public void F02_TC01() throws NotAbleToCreateTestException {
        appController.createNewTest();
    }

    @Test(expected = asir2036MV.exception.NotAbleToCreateTestException.class)
    public void F02_TC02() throws NotAbleToCreateTestException, DuplicateIntrebareException, InputValidationFailedException {
        Intrebare intrebare1 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Boli");
        Intrebare intrebare2 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "2", "Boli");
        Intrebare intrebare3 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "3", "Boli");
        Intrebare intrebare4 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "2", "Celule");
        Intrebare intrebare5 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Alimentatie sanatoasa");
        appController.addNewIntrebare(intrebare1);
        appController.addNewIntrebare(intrebare2);
        appController.addNewIntrebare(intrebare3);
        appController.addNewIntrebare(intrebare4);
        appController.addNewIntrebare(intrebare5);

        appController.createNewTest();
    }

    @Test
    public void F02_TC03() throws NotAbleToCreateTestException, DuplicateIntrebareException, InputValidationFailedException {
        Intrebare intrebare1 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Boli");
        Intrebare intrebare2 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "2", "Organe externe");
        Intrebare intrebare3 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Organe interne");
        Intrebare intrebare4 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "2", "Celule");
        Intrebare intrebare5 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Alimentatie sanatoasa");
        appController.addNewIntrebare(intrebare1);
        appController.addNewIntrebare(intrebare2);
        appController.addNewIntrebare(intrebare3);
        appController.addNewIntrebare(intrebare4);
        appController.addNewIntrebare(intrebare5);

        asir2036MV.model.Test test = appController.createNewTest();
        assert test.getIntrebari().size() == 5;
    }
}
