package asir2036MV.controller;

import asir2036MV.exception.DuplicateIntrebareException;
import asir2036MV.exception.InputValidationFailedException;
import asir2036MV.exception.NotAbleToCreateStatisticsException;
import asir2036MV.exception.NotAbleToCreateTestException;
import asir2036MV.model.Intrebare;
import asir2036MV.model.Statistica;
import asir2036MV.repository.IntrebariRepository;
import asir2036MV.util.InputValidation;
import asir2036MV.view.UserInterface;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.*;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class BigBangTest {
    private String file;
    private IntrebariRepository intrebariRepository;
    private AppController appController;
    private InputValidation inputValidation;
    private UserInterface userInterface;


    @Before
    public void setUp() throws Exception {
        file = "test.txt";
        this.intrebariRepository = new IntrebariRepository(file);
        this.inputValidation = new InputValidation();
        this.appController = new AppController(intrebariRepository, inputValidation);
        userInterface = new UserInterface(appController);

    }

    @After
    public void tearDown() throws Exception {
        PrintWriter writer = new PrintWriter(file);
        writer.print("");
        writer.close();
    }

    @Test
    public void f01_addIntrebare() throws InputValidationFailedException, DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Celule");

        Intrebare added = appController.addNewIntrebare(intrebare);
        assertEquals(added.getEnunt(), intrebare.getEnunt());
        assertEquals(added.getVarianta1(), intrebare.getVarianta1());
        assertEquals(added.getVarianta2(), intrebare.getVarianta2());
        assertEquals(added.getVarianta3(), intrebare.getVarianta3());
        assertEquals(added.getVariantaCorecta(), intrebare.getVariantaCorecta());

    }
    @Test
    public void f02_createTest() throws NotAbleToCreateTestException, DuplicateIntrebareException, InputValidationFailedException {
        Intrebare intrebare1 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Boli");
        Intrebare intrebare2 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "2", "Organe externe");
        Intrebare intrebare3 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Organe interne");
        Intrebare intrebare4 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "2", "Celule");
        Intrebare intrebare5 = new Intrebare("Care e celula ceaa mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Alimentatie sanatoasa");

        appController.addNewIntrebare(intrebare1);
        appController.addNewIntrebare(intrebare2);
        appController.addNewIntrebare(intrebare3);
        appController.addNewIntrebare(intrebare4);
        appController.addNewIntrebare(intrebare5);
        appController.createNewTest();

    }
    @Test
    public void tcPass() throws NotAbleToCreateStatisticsException, DuplicateIntrebareException, InputValidationFailedException {

        Intrebare intrebare1 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Boli");
        Intrebare intrebare2 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "2", "Organe externe");
        Intrebare intrebare3 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Organe interne");
        Intrebare intrebare4 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "2", "Celule");
        Intrebare intrebare5 = new Intrebare("Care e celuula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Alimentatie sanatoasa");

        appController.addNewIntrebare(intrebare1);
        appController.addNewIntrebare(intrebare2);
        appController.addNewIntrebare(intrebare3);
        appController.addNewIntrebare(intrebare4);
        appController.addNewIntrebare(intrebare5);
        Statistica s = appController.getStatistica();
        Assert.assertEquals(s.getIntrebariDomenii().size(),5);
    }

    @Test
    public void integration_test() {

        try {


            userInterface.bufferedReader = Mockito.mock(BufferedReader.class);

            Mockito.when(userInterface.bufferedReader.readLine()).thenReturn("1", "Care e celula cea mai putin raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Celule",
                    "1", "Care e celula cea mai putin raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Alimentatie sanatoasa",
                    "1", "Care e celula cea mai putin raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Organe interne",
                    "1", "Care e celula cea mai putin raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Organe externe",
                    "1", "Care e celula cea mai putin raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Boli",
                    "2",
                    "3",
                    "4");
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            System.setOut(new PrintStream(bo));
            bo.flush();
            userInterface.show();
            String allWrittenLines = new String(bo.toByteArray());

            assertTrue(allWrittenLines.contains("Added"));
            assertTrue(allWrittenLines.contains("Testul generat este:"));
            assertTrue(appController.getStatistica().getIntrebariDomenii().size() == 5);


        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotAbleToCreateStatisticsException e) {
            e.printStackTrace();
        }


    }

}
